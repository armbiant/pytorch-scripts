"""Adapted from the pycapture example SaveImageToAVIEx.py."""
import argparse
import os
from tqdm import tqdm
import datetime
from glob import glob
import PyCapture2
from pycapture_utils import printBuildInfo
from pycapture_utils import printCameraInfo


def saveAviHelper(cam, file_format, video_name, frame_rate, number_of_frames,
				  time_stamps_file):
	avi = PyCapture2.AVIRecorder()
	counter = 0
	for i in tqdm(range(number_of_frames), desc="saving video..."):
		try:
			image = cam.retrieveBuffer()
		except PyCapture2.Fc2error as fc2Err:
			print("Error retrieving buffer : ", fc2Err)
			continue
		if (i % 500 == 0):
			if counter > 0:
				avi.close()
				avi = PyCapture2.AVIRecorder()
			file_name = video_name + '_' + str(counter) + '.avi'
			if file_format == "AVI":
				avi.AVIOpen(file_name.encode("utf-8"), frame_rate)
			elif file_format == "MJPG":
				avi.MJPGOpen(file_name.encode("utf-8"), frame_rate, 100)
			elif file_format == "H264":
				avi.H264Open(file_name.encode("utf-8"), frame_rate, image.getCols(),
							 image.getRows(), 1000000)
			else:
				return

			counter += 1

		avi.append(image)
		time_stamps_file.write(str(datetime.datetime.now()) + '\n') # to recover datetime object date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S.%f')
	time_stamps_file.close()

	print("Appended {} images to {} file: {}...".format(number_of_frames, file_format, file_name))



def main(args):
	printBuildInfo()
    # Ensure sufficient cameras are found
	bus = PyCapture2.BusManager()
	numCams = bus.getNumOfCameras()
	print("Number of cameras detected: ", numCams)
	if not numCams:
		print("Insufficient number of cameras. Exiting...")
		exit()
    # Select camera on 0th index
	cam = PyCapture2.Camera()
	cam.connect(bus.getCameraFromIndex(0))
    # Print camera details
	printCameraInfo(cam)
	print("Starting capture...")
	cam.startCapture()
	print("Detecting frame rate from Camera")
	fRateProp = cam.getProperty(PyCapture2.PROPERTY_TYPE.FRAME_RATE)
	frame_rate = fRateProp.absValue
	number_of_frames = int(args.recording_time * frame_rate)
	print("Using frame rate of {}".format(frame_rate))
	time_stamps_path = os.path.join(args.save_folder, 'timestamps.txt')  # name of my log file
	if not os.path.isfile(time_stamps_path):
		time_stamps_file = open(time_stamps_path, "w+")
	print("Saving timestamps to")
    # for file_format in ("AVI","H264","MJPG"):
	video_name = os.path.join(args.save_folder, args.file_name)
	saveAviHelper(cam, args.file_format, video_name, frame_rate,
                  number_of_frames, time_stamps_file)
	print("Stopping capture...")
	cam.stopCapture()
	cam.disconnect()
	rename_files(args.save_folder, args.file_name)


def rename_files(save_folder, file_name):
	video_files = [f for f in glob(os.path.join(save_folder, '*'))
					if (file_name in f) and ('.avi' in f)]
	for video_file in video_files:
		root_folder = os.path.split(video_file)[0]
		name = os.path.split(video_file)[1]
		new_name = name[:-9] + '.avi'
		new_video_file = os.path.join(root_folder, new_name)
		os.rename(video_file, new_video_file)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	videos_folder = os.path.join(os.getenv("HOME"), 'Videos', 'test')
	parser.add_argument("--save_folder", "-sf", help="full path where to save the video.", type=str, default=videos_folder)
	parser.add_argument("--file_name", "-fn", help="name of the video file", type=str, default='test')
	parser.add_argument("--file_format", "-ff", help="format to save the video: AVI (raw), MJPG (default) or H264", type=str, default='MJPG')
	parser.add_argument("--recording_time", "-rt", help="recording time in seconds (30 sec. by default)", type=int, default=30)
	args = parser.parse_args()
	print("***********************************")
	print("Saving video in ", args.save_folder)
	record_video = True
	if not os.path.isdir(args.save_folder):
		print("Creating save_folder...")
		os.mkdir(args.save_folder)
	elif args.file_name + '.avi' in os.listdir(args.save_folder):
		overwrite = ''
		while not (overwrite == 'n' or overwrite == 'y'):
			overwrite = input('A file with the name "%s" already exist, do yo want to overwrite it? [n/y]' % args.file_name)
			print(overwrite)
			if overwrite == 'y':
				print("The video will be overwritten")
			if overwrite == 'n':
				print("Please, run again the script and select a different file name")
				record_video = False

	if record_video:
		print("Video name: ", args.file_name)
		print("File format: ", args.file_format)
		print("Recording time (sec): ", args.recording_time)
		main(args)
