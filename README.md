# Recording with PyCapture2
This repository includes a small example of how to use the library
*PyCapture2* to record videos using Point Grey cameras using Python.

The code on this repository is inspired by the examples provided in
*PyCapture2*.

# Requirements

The *FlyCapture SDK* and the python library *PyCapture2* must be installed.
Both softwares and the instructions for its installation can be found in
[downloads page of Point Grey](https://www.ptgrey.com/support/downloads)
